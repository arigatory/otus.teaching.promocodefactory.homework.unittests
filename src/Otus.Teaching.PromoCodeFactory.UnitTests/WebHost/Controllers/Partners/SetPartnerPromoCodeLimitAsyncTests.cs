﻿using System;
using Xunit;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Moq;
using AutoFixture.AutoMoq;
using AutoFixture;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }



        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        //TODO: Add Unit Tests
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = 10 };


            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsBlocked_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = 10 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_SetToZeroNumberOfPromocodes()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = 10 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_CancelPreviousLimit()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = -1 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_ShouldBeGreaterThanZero()
        {
            // Arrange
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = 10 };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //[Fact]
        //public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_ShouldSaveToDB()
        //{
        //    // Arrange
        //    var builder = new DbContextOptionsBuilder();
        //    builder.UseInMemoryDatabase("CanAddPromoCode");
        //    var partner = CreateBasePartner();
        //    partner.IsActive = false;
        //    var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
        //    var request = new PromoCodeFactory.WebHost.Models.SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Today.AddDays(1), Limit = 10 };

        //    _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
        //        .ReturnsAsync(partner);

        //    // Act
        //    var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

        //    // Assert
        //    Assert.NotEqual(0, request.Limit);
        }
    }
}